// imports des dépendances
import * as PIXI from "pixi.js";
import { TweenMax, PixiPlugin } from "gsap/all";
const gsapPlugins = [TweenMax, PixiPlugin];
PixiPlugin.registerPIXI(PIXI);
import marioUrl from "./assets/mario.jpg";
import mario2Url from "./assets/mario2.jpg";
import { createSpriteMenu } from "./components/menu";
import { createSpriteLifes } from "./components/lifes";

// Classe de jeu principale
class MyGame {
  // au démarrage
  constructor() {
    this.app = new PIXI.Application({
      width: 800,
      height: 600
    });
    document.body.appendChild(this.app.view);

    this.state = {
      current: "niveau1",
      isMenuOpened: false
    };

    this.app.loader
      .add("mariobg", marioUrl)
      .add("mariobg2", mario2Url)
      .load((loader, resources) => {
        this.init({ loader, resources });
      });
  }

  /**
   * Initialize le jeu
   */
  init({ loader, resources }) {
    // créé le niveau1
    this.niveau1 = new PIXI.Sprite(resources.mariobg.texture);
    this.app.stage.addChild(this.niveau1);

    // créé le niveau2
    this.niveau2 = new PIXI.Sprite(resources.mariobg2.texture);
    this.app.stage.addChild(this.niveau2);

    // créé la barre de vie
    this.lifes = createSpriteLifes({ app: this.app, loader });
    this.app.stage.addChild(this.lifes);

    // créé le menu global
    this.menu = createSpriteMenu();
    this.app.stage.addChild(this.menu);

    // affiche le niveau 1 et le menu
    this.showNiveau(1);
    this.manageMenuVisibility({ instant: true, isDisplayed: false });

    // ajoute les écouteurs d'évènements
    this.addListeners();

    // boucle de jeu
    this.app.ticker.add(() => {});
  }

  addListeners() {
    // évènement de clavier
    document.addEventListener("keyup", event => {
      if (event.defaultPrevented) return;
      switch (event.key) {
        case "Escape":
          this.manageMenuVisibility();
          break;
      }
    });

    // événement de jeu
    document.addEventListener("clickMenu", event => {
      if (this.state.isMenuOpened) {
        const buttonName = event.detail.buttonName;
        switch (buttonName) {
          // si click sur bouton niveau1
          case "niveau1":
            this.showNiveau(1);
            this.manageMenuVisibility({ isDisplayed: false });
            break;

          // si click sur bouton niveau2
          case "niveau2":
            this.showNiveau(2);
            this.manageMenuVisibility({ isDisplayed: false });
            break;

          // si click sur bouton exit
          case "exit":
            this.manageMenuVisibility({ isDisplayed: false });
            break;
          default:
        }
      }
    });
  }

  // afficher le niveau demandé
  showNiveau(num) {
    if (num === 1) {
      this.niveau2.alpha = 0;
      this.niveau1.alpha = 1;
    } else if (num === 2) {
      this.niveau1.alpha = 0;
      this.niveau2.alpha = 1;
    }
    // met a jour l'état global du jeu avec le niveau en cours
    this.state = {
      ...this.state,
      current: `niveau${num}`
    };
  }

  // affiche/cache le menu de jeu
  manageMenuVisibility(
    { isDisplayed = true, instant = false } = {
      isDisplayed: true,
      instant: false
    }
  ) {
    if (instant) {
      this.menu.visible = isDisplayed;
    } else {
      TweenMax.to(this.menu, 0.25, {
        pixi: {
          alpha: isDisplayed ? 1 : 0,
          onComplete: () => {
            this.menu.visible = isDisplayed;
          }
        }
      });
    }
    // met a jour l'état global du jeu avec l'information de visibilité du menu
    this.state = {
      ...this.state,
      isMenuOpened: isDisplayed
    };
  }
}

// créé mon jeu pour le démarrer
const myGame = new MyGame();
