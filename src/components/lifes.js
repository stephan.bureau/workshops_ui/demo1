import * as PIXI from "pixi.js";
import hearthUrl from "../assets/heart.png";
import { create } from "domain";

// déclare et défini les variables
let lifes = [{}, {}, {}, {}],
  lifesContainer;

// se déclenche quand le joueur pert une vie
export const removeLife = () => {
  lifes.pop();
};

// se déclenche quand le joueur gagne une vie
export const addLife = () => {
  lifes.push(createLifeSprite(lifes.length + 1));
};

// créé un template de création de vie
const createLifeSprite = (resources, key) => {
  const life = new PIXI.Sprite(resources.heartUrl.texture);
  life.anchor.set(0.5);
  life.scale.set(0.5);
  life.x += key * 50;
  lifesContainer.addChild(life);
  return life;
};

// créé la barre de vie et ses éléments graphiques
export const createSpriteLifes = ({ app, loader }) => {
  lifesContainer = new PIXI.Container();
  lifesContainer.x = app.screen.width - 40 * 4 - 40;
  lifesContainer.y = app.screen.height - 60;

  loader.add("heartUrl", hearthUrl).load((_, resources) => {
    lifes.map((__, key) => createLifeSprite(resources, key));
  });

  return lifesContainer;
};
