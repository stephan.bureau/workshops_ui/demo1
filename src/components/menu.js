import * as PIXI from "pixi.js";

// déclare mes boutons
let niveau1Text, niveau2Text, exitText;

// se déclenche lorsque le curseur passe au dessus d'un bouton
const onPointerOver = event => {
  event.currentTarget.style = styleHover;
};

// se déclenche lorsque le curseur sort d'un bouton
const onPointerOut = event => {
  event.currentTarget.style = style;
};

// se déclenche lorsque le curseur clique sur un bouton
const onPointerDown = event => {
  switch (event.currentTarget) {
    // si bouton niveau1 est cliqué
    case niveau1Text:
      document.dispatchEvent(
        new CustomEvent("clickMenu", { detail: { buttonName: "niveau1" } })
      );
      break;

    // si bouton niveau2 est cliqué
    case niveau2Text:
      document.dispatchEvent(
        new CustomEvent("clickMenu", { detail: { buttonName: "niveau2" } })
      );
      break;

    // si bouton exit est cliqué
    case exitText:
      document.dispatchEvent(
        new CustomEvent("clickMenu", { detail: { buttonName: "exit" } })
      );
      break;
  }
};

// style du text à appliquer
const style = new PIXI.TextStyle({
  fontFamily: "Arial",
  fontSize: 36,
  // fontStyle: "italic",
  fontWeight: "bold",
  fill: ["#ffffff", "#fc0"], // gradient
  stroke: "#000",
  strokeThickness: 4,
  dropShadow: true,
  dropShadowColor: "#000000",
  dropShadowBlur: 6,
  dropShadowAngle: Math.PI / 6,
  dropShadowDistance: 3,
  wordWrap: true,
  wordWrapWidth: 440
});

// style du text quand le curseur passe au dessus
const styleHover = { ...style, fill: ["magenta"] };

// ajoute les intéractions possibles avec les boutons
const addInteractions = button => {
  button.interactive = true;
  button.buttonMode = true;
  button
    .on("pointerdown", onPointerDown)
    // .on("pointerup", onPointerUp)
    // .on("pointerupoutside", onButtonUp)
    .on("pointerover", onPointerOver)
    .on("pointerout", onPointerOut);
};

// créé le menu avec ses éléments graphiques
export const createSpriteMenu = () => {
  const menu = new PIXI.Container();

  const background = new PIXI.Graphics();
  background.alpha = 0.7;
  background.beginFill(0xffffff);
  background.drawRect(0, 0, 800, 600);
  background.endFill();
  menu.addChild(background);

  const menuContainer = new PIXI.Graphics();
  menuContainer.alpha = 1;
  menuContainer.beginFill(0xffffff);
  menuContainer.drawRect(300, 150, 200, 300);
  menuContainer.endFill();
  menu.addChild(menuContainer);

  niveau1Text = new PIXI.Text("Niveau1", style);
  niveau1Text.x = 325;
  niveau1Text.y = 160;
  addInteractions(niveau1Text);
  menu.addChild(niveau1Text);

  niveau2Text = new PIXI.Text("Niveau2", style);
  niveau2Text.x = 325;
  niveau2Text.y = 240;
  addInteractions(niveau2Text);
  menu.addChild(niveau2Text);

  exitText = new PIXI.Text("Exit", style);
  exitText.x = 360;
  exitText.y = 380;
  addInteractions(exitText);
  menu.addChild(exitText);

  return menu;
};
